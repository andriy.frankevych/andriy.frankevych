# 👋 Hi!

I'm a backend developer at team Shape.

## Working Style

I am a big fan of remote work and have been working from home since 2020.

### Communication

If something can be communicated asynchronously it should be communicated asynchronously. I don't expect anyone to respond immediately and I assume that I'm not expected to respond immediately unless it's required by the context.

You can reach me on slack by searching my name 😉

### Productivity

Normally my workday consists of the following phases:

- Catching up and planning
- Deep work (preferably in the morning)
- Collaborative work

I'm a big fan of the Pomodoro technique, GTD system, and Inbox Zero method. 

In software development I try to adhere to the following principles:

- Minimalism
- Scalability
- Easiness to throw away

## Areas of improvement
 
- *Focus*: I tend to lose interest pretty quickly and have a giant cemetery of abandoned books, courses, side projects, and hobbies that I'm trying to declutter
- *Communication*: Working on my spoken skills since I've always struggled to deliver my ideas in a structured, concise, and fluent way 

## WFH Setup

### Software

- Dotfiles manager: [Chezmoi](https://www.chezmoi.io/)
- IDE: [JetBrains WebStorm](https://www.jetbrains.com/webstorm/)
- Terminal: [iTerm2](https://iterm2.com/)
- Shell: [Zsh](https://www.zsh.org/) with [Oh My Zsh](https://ohmyz.sh/)
- Window manager: [Amethyst](https://ianyh.com/amethyst/)
- Notes and the second brain: [Obsidian](https://obsidian.md/)
- Shortcuts: [Alfred](https://www.alfredapp.com/)

(Almost) Every software I use has the [Dracula Theme](https://draculatheme.com/)

### Hardware
  
- Computer: [MacBook Pro 16" 2021 with Apple M1 Pro](https://www.apple.com/it/macbook-pro-14-and-16/)
- Display: [Ben-Q GW2780T 27"](https://www.benq.com/en-us/monitor/home/gw2780t.html)
- Keyboard: [Keychron K6 Pro](https://www.keychron.com/collections/all-keyboards/products/keychron-k6-pro-qmk-via-wireless-custom-mechanical-keyboard)
- Mouse: [MX Master 3s](https://www.logitech.com/it-ch/products/mice/mx-master-3s.910-006559.html)
- Headphones: [Sony WH-CH510](https://electronics.sony.com/audio/headphones/all-headphones/p/whch510-b) that I'm going to upgrade to [WH-1000XM5](https://electronics.sony.com/audio/headphones/all-headphones/p/wh1000xm5-b)s at some point
- USB-C Hub: [Emtech T650C](https://www.emtec-international.com/en/mobility/hubs/t650c-type-c-hub)
- Webcam: [Logitech c922 Pro HD](https://www.logitech.com/it-it/products/webcams/c922-pro-stream-webcam.960-001088.html)
- Ringlight: [Neewer 14"](https://it.neewer.com/collections/ring-light/products/led-ring-lights-66600021)
- Speakers that I rarely use: [Logitech Z150](https://www.logitech.com/en-us/products/speakers/z150-compact-stereo-speakers.980-000802.html)
- Desk: [Ikea Skarsta](https://www.ikea.com/it/it/p/trotten-scrivania-regolabile-in-altezza-bianco-s99429578/) standing desk
- Chair I'm trying to use less: [Ikea Renberget](https://www.ikea.com/it/it/p/renberget-sedia-girevole-bomstad-nero-60493546/)

## Trivia

- I was born in Ukraine and my family moved to Italy when I was 14 where I've been living ever since
- Pescetarian
- I strongly believe that fighting climate change should be considered our top priority
- Former parkour practitioner and coach
- Amateur runner
- Recently fell in love with SUP
